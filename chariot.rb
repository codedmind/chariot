require "gtk2"
require "gtksourceview2"
require "libkaiser"
require "codedmind"
require 'json'

class Chariot
  def redirect(&fn)
    @redirect = fn
  end
  
  def initialize
    #if not File.exist?("#{$ROOTDIR}/configs/"); Dir.mkdir("#{$ROOTDIR}/configs/"); end
    if not File.exist?("#{$ROOTDIR}/extensions/"); Dir.mkdir("#{$ROOTDIR}/extensions/"); end
    if not File.exist?("#{$ROOTDIR}/brains/"); Dir.mkdir("#{$ROOTDIR}/brains/"); end
    @redirect = nil
    @root_window = Gtk::Window.new
    @root_window.set_size_request(350, 150)
    @root_window.signal_connect('destroy') { Gtk.main_quit }
    @brain = CodedMind.new
    @brain.load_directory "#{$ROOTDIR}/brains/"

    @extensions = {}
    @extmenus = {}
    
    if not File.exist?("#{$ROOTDIR}/chariot.json")
      File.open("#{$ROOTDIR}/chariot.json", "w+") { |file| file.write("{}") }
      @config = {}
      @config["theme"] = {}
      @config["theme"]["bot"] = "Athena"
      @config["theme"]["bot_colour"] = "5555ff"
      @config["theme"]["user"] = "user"
      @config["theme"]["user_colour"] = "ff5555"
      @config["theme"]["main_txt_font"] = "Monospace 9"
      @config["internals"] = {}
      @config["internals"]["blacklist"] = []
      @config["internals"]["autoloads"] = []
      writeConfig
    else
      @config = JSON.parse(File.read("#{$ROOTDIR}/chariot.json"))
    end
    @theme = @config["theme"]
  end

  attr_reader :brain
  def writeConfig
    File.open("#{$ROOTDIR}/chariot.json", "w+") { |file|
      file.write(@config.to_json)
    }
  end
  def autoloadExtension(ext_id)
    @config["internals"]["autoloads"].append ext_id
    writeConfig
  end
  
  def unautoloadExtension(ext_id)
    @config["internals"]["autoloads"].delete ext_id
    writeConfig
  end
  
  def blacklistExtension(ext_id)
    @config["internals"]["blacklist"].append ext_id
    writeConfig
  end
  
  def unblacklistExtension(ext_id)
    @config["internals"]["blacklist"].delete ext_id
    writeConfig
  end
  
  def setv(grp, key, value)
    unless @config[grp].key? key
      @config[grp] = {}
    end
    @config[grp][key] = value
    writeConfig
  end
  
  def getv(grp, key, default)
    @config[grp] = {} unless @config.key? grp
    if @config[grp].key? key
      return @config[grp][key]
    end
    default
  end
  
  def openExtensionManager
    @extwin = Gtk::Window.new
    @extwin.set_size_request(350, 150)
    @extlay = Gtk::ScrolledWindow.new
    @extlay.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS)
    @hb = Gtk::VBox.new
    @extlay.add_with_viewport @hb
    
    Dir.foreach("#{$ROOTDIR}/extensions/") do |entry|
      if entry.end_with? ".rb"
        e = Gtk::CheckButton.new(entry.split(".")[0])
        if @extensions[entry.split(".")[0]][:enabled]
          e.active = true
        end
        e.signal_connect( "toggled" ) { |w|
          if w.active? #turn off ext
            send "load_#{w.label}", self
            @extensions[entry.split(".")[0]][:enabled] = true
          else
            send "unload_#{w.label}", self
            @extensions[entry.split(".")[0]][:enabled] = false
          end
        }
        @hb.add e
      end
    end
    

    @extwin.add @extlay
    @extwin.show_all
  end

  def loadExtensions
    blacklist = []
    autos = []
    blacklist = @config["internals"]["blacklist"] unless @config["internals"]["blacklist"] == nil
    autos = @config["internals"]["autoloads"] unless @config["internals"]["autoloads"] == nil
    Dir.foreach("#{$ROOTDIR}/extensions/") do |entry|
      if entry.end_with? ".rb"
        puts "Loading extension #{entry} in to memory..."

        unless blacklist.include? entry.split(".")[0]
          @extensions[entry.split(".")[0]] = {ui_elements: {}, enabled: false}
          require entry.split(".")[0]

          if autos.include? entry.split(".")[0]
            send "load_#{entry.split(".")[0]}", self
            @extensions[entry.split(".")[0]][:enabled] = true
          end
        end
      end
    end
  end

  def openEval
    if @codewin != nil and @coutwin != nil
      @codewin.destroy
      @coutwin.destroy
    else
      @codewin = Gtk::Window.new
      @codes = Gtk::ScrolledWindow.new
      ruby_lang = Gtk::SourceLanguageManager.new.get_language('ruby')
      revalbuf = Gtk::SourceBuffer.new ruby_lang
      @cbox = Gtk::SourceView.new revalbuf
      @cbox.show_line_marks = true
      @cbox.show_line_numbers = true
      
      
      @cbox.wrap_mode = Gtk::TextTag::WRAP_WORD
      @codes.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
      @coutwin = Gtk::Window.new
      @couts = Gtk::ScrolledWindow.new
      @cout = Gtk::TextView.new
      @cout.wrap_mode = Gtk::TextTag::WRAP_WORD
      @couts.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
      @codewin.title = "Live Evaluation"
      @codewin.set_size_request(500, 450)
      @coutwin.set_size_request(450, 200)
      @codewin.signal_connect('destroy') { @codewin = nil }
      @code = Gtk::Table.new 5, 6, false
      @cout.buffer.create_tag("err",           {"underline"     => Pango::Underline::SINGLE, "foreground" => "#ff0000"})
      

      @coutwin.add @couts
      @couts.add @cout
      @coutwin.title = "Eval Output"
      @coutwin.show_all
      
      @codewin.add @codes
      @codes.add @cbox
      #@code.attach @codes, 0, 2, 1, 6, Gtk::EXPAND|Gtk::FILL, Gtk::EXPAND|Gtk::FILL, 1, 1
      @codewin.show_all
      
      @cbox.signal_connect('key_press_event') { |w, e|
        if e.keyval == 65474
          begin
            result = eval @cbox.buffer.text
            @cout.buffer.text += "#{result}\n"
          rescue Exception => e  
            @cout.buffer.text += e.message+"\n"
            tag @cout, "#{e.message}", "err"
            for bt in e.backtrace
              @cout.buffer.text += "-- #{bt}\n"
            end
          end
          @cout.buffer.text += "---------------\n"
        end
      }
    end 
  end

  
  def refresh
    @root_window.signal_connect('key_press_event') { |w, e|
      if e.keyval == 65473
        openEval
      end
    }
    @main = Gtk::Table.new 10, 6, false
    @entry = Gtk::Entry.new
    @send = Gtk::Button.new "Send"
    @entry.signal_connect('key_press_event') { |w, e|
      if e.keyval == 65293;sendToBot;end; }
    @txt = Gtk::TextView.new
    @txts = Gtk::ScrolledWindow.new
    @txts.border_width = 5
    @txts.add(@txt)
    @txts.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS)
    @txt.wrap_mode = Gtk::TextTag::WRAP_WORD
    @txt.editable = false
    changeTxtFont(@txt, @theme["main_txt_font"])
    # Placing
    @main.attach @txts, 0, 10, 1, 5, Gtk::FILL|Gtk::EXPAND, Gtk::FILL|Gtk::EXPAND
    @main.attach @entry, 0, 9, 5, 6, Gtk::EXPAND|Gtk::FILL, Gtk::SHRINK
    @main.attach @send, 9, 10, 5, 6, Gtk::SHRINK, Gtk::SHRINK
    @root_window.add @main

    # Menu

    menubar = Gtk::MenuBar.new

    file = Gtk::MenuItem.new("File")
    edit = Gtk::MenuItem.new("Edit")
    ext = Gtk::MenuItem.new("Extend")
    
    help = Gtk::MenuItem.new("Help")
    filemenu = Gtk::Menu.new
    editmenu = Gtk::Menu.new
    extmenu = Gtk::Menu.new
    @ext_menu = extmenu
    helpmenu = Gtk::Menu.new
    file.submenu = filemenu
    edit.submenu = editmenu
    ext.submenu = extmenu
    help.submenu = helpmenu
    menubar.append(file)
    menubar.append(edit)
    menubar.append(ext)
    menubar.append(help)
    
    extb = Gtk::MenuItem.new("Extensions")
    extb.signal_connect('activate') { |w| openExtensionManager }
    extmenu.append extb

    extev = Gtk::MenuItem.new("Live Console")
    extev.signal_connect('activate') { |w| openEval }
    extmenu.append extev

    vbox = Gtk::VBox.new(homogeneous = false, spacing = nil)
    vbox.pack_start_defaults(menubar)
    #vbox.pack_start_defaults(label)
    @main.attach vbox, 0, 10, 0, 1, Gtk::EXPAND|Gtk::FILL, Gtk::SHRINK

    
    # Events
    @send.signal_connect('clicked') {
      sendToBot
    }

    # Txt View tags
    @txt.buffer.create_tag("bold",          {"weight"        => Pango::Weight::BOLD})
    @txt.buffer.create_tag("italic",        {"style"         => Pango::Style::ITALIC})
    @txt.buffer.create_tag("strikethrough", {"strikethrough" => true})
    @txt.buffer.create_tag("underline",     {"underline"     => Pango::Underline::SINGLE})
    @txt.buffer.create_tag("err",           {"underline"     => Pango::Underline::SINGLE, "foreground" => "#ff0000"})
    @txt.buffer.create_tag("self",          { "foreground"   => "#"+@theme["bot_colour"],     "weight" => Pango::Weight::BOLD })
    @txt.buffer.create_tag("user",          { "foreground"   => "#"+@theme["user_colour"],    "weight" => Pango::Weight::BOLD })

    # Begin Render
    @root_window.show_all
  end

  def addExtButton(extension_id, name, &fn)
    n = Gtk::MenuItem.new(name)
    n.signal_connect('activate') { |w| fn.call(self) }
    @ext_menu.append n
    n.show
    @extensions[extension_id][:ui_elements]["mb_"+name] = n
  end
  
  def delExtButton(extension_id, name)
    @extensions[extension_id][:ui_elements]["mb_"+name].destroy()
    @extensions[extension_id][:ui_elements]["mb_"+name] = nil
  end
  
  def changeTxtFont(txt, fontname)
    font = Pango::FontDescription.new(font)
    txt.modify_font(font)
  end
  
  def evalWinOutput(str)
    puts str
  end
  
  def sendToBot
    @txt.buffer.text += "\n[#{@theme["user"]}] "+@entry.text
    
    if @redirect != nil
      redreply = @redirect.call(@entry.text)
      if redreply != nil and redreply.strip != ""
        output redreply
      end
    else
      reply = @brain.reply "user", @entry.text, false
      if reply != nil and reply.strip != ""
        say reply
      end     
    end
    
    @entry.text = ""
  end

  def say(txt)
    @txt.buffer.text += "\n[#{@theme["bot"]}] "+txt
    tag @txt, @theme["user"], "user"
    tag @txt, @theme["bot"], "self"
  end
  
  def output(txt)
    @txt.buffer.text += "\n"+txt
    tag @txt, @theme["user"], "user"
    tag @txt, @theme["bot"], "self"
  end
  
  def tag(textview, search, tag)
    eob_mark = textview.buffer.create_mark(nil,textview.buffer.start_iter.forward_to_end,false)
    textview.scroll_mark_onscreen(eob_mark)

    start = textview.buffer.start_iter
    r = start.forward_search(search, Gtk::TextIter::SEARCH_TEXT_ONLY, nil)

    while (r)
      start.forward_char
      r = start.forward_search(search, Gtk::TextIter::SEARCH_TEXT_ONLY, nil)
      break if r == nil
      start = r[0]
      textview.buffer.apply_tag(tag, r[0], r[1])
    end
  end
  
  def begin
    @txt.buffer.text = "[#{@theme["bot"]}] Hello, my name is #{@theme["bot"]}. I'll be your assistant."
    tag @txt, @theme["bot"], "self"
    Gtk.main
  end
end
