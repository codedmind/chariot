
def ask(title, label, &fn)
  prompt = Gtk::Window.new
  prompt.title = title
  v = Gtk::VBox.new
  v.add Gtk::Label.new(label)
  
  items = Gtk::HBox.new
  prompt_yes = Gtk::Button.new "Yes"
  prompt_no = Gtk::Button.new "No"

  v.add items
  items.add prompt_yes
  items.add prompt_no

  prompt.add v
  prompt_yes.signal_connect('clicked') {
    fn.call true
    prompt.destroy()
  }
  prompt_no.signal_connect('clicked') {
    fn.call false
    prompt.destroy()
  }
  prompt.show_all
end

def openPrompt(title, label, &fn)
  prompt = Gtk::Window.new
  prompt.title = title
  v = Gtk::VBox.new
  v.add Gtk::Label.new(label)
  
  items = Gtk::HBox.new
  prompt_entry = Gtk::Entry.new
  prompt_send = Gtk::Button.new "Confirm"

  v.add items
  items.add prompt_entry
  items.add prompt_send

  prompt.add v
  prompt_send.signal_connect('clicked') {
    fn.call prompt_entry.text
    prompt.destroy()
  }
  prompt.show_all
end

def askDir(prompt = "Open directory", root=Dir.home, &fn)
  dialog = Gtk::FileChooserDialog.new(prompt, @root_window, Gtk::FileChooser::ACTION_SELECT_FOLDER, nil,
                                      [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
                                      [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
  dialog.current_folder = root
  if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT;
    fn.call dialog.filename;
  end;
  dialog.destroy;
end


def addToTree(ls, string, base = nil); ls.model.append(base)[0] = string; end

class HashEditor
  def initialize(data, title = "Editor")
    @data = data
    @window = Gtk::Window.new
    @window.title = title
    @lines = Gtk::VBox.new
    @controls = Gtk::HBox.new
    @add = Gtk::Button.new "+"
    @save = Gtk::Button.new "Save"
    @controls.add @add;@controls.add @save;
    @window.add @lines
    @lines.add @controls

    @linesc = {}
    
    data.each { |k, v|
      addRow k, v
    }

    @add.signal_connect('clicked') {
      openPrompt("New key", "Enter new key: ") { |key| 
        addRow key
      }
      
    }

    @save.signal_connect('clicked') {
      out = {}
      @linesc.each { |k, v|
        out[v[:entry_key].text] = v[:entry_val].text
      }
      puts out
    }
    
    @window.show_all
  end

  def addRow(key, value="")
    @linesc[key] = {type: value.class, container: Gtk::HBox.new}
    @linesc[key][:entry_key] = Gtk::Entry.new
    @linesc[key][:entry_val] = Gtk::Entry.new
    @linesc[key][:button] = Gtk::Button.new "-"
    @lines.add @linesc[key][:container]
    @linesc[key][:container].add @linesc[key][:entry_key]
    @linesc[key][:container].add @linesc[key][:entry_val]
    @linesc[key][:container].add @linesc[key][:button]
    @linesc[key][:entry_key].text = key
    @linesc[key][:entry_val].text = value

    @linesc[key][:button].signal_connect('clicked') {
      @linesc[key][:container].destroy
      @linesc.delete key
      puts @linesc
    }
    @linesc[key][:container].show_all
  end

  def removeRow(key)
  end
end
